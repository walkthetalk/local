package client;

import core.RequestEntity;
import core.ResponseEntity;
import handler.SampleHandlerTwo;
import handler.SamplePayload;

public class HandlerTwoLocalClient implements HandlerTwoClient {

    private SampleHandlerTwo handlerTwo;

    public HandlerTwoLocalClient(SampleHandlerTwo handlerTwo) {
        this.handlerTwo = handlerTwo;
    }

    public ResponseEntity<SamplePayload> call(RequestEntity<SamplePayload> input) {
        return handlerTwo.handleInternal(input);
    }
}
