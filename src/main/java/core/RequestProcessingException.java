package core;

public abstract class RequestProcessingException extends RuntimeException {
    public abstract int getResponseCode();
    public abstract String getErrorMessage();
}
