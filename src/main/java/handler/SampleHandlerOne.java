package handler;

import client.HandlerTwoClient;
import core.AbstractHandler;
import core.RequestEntity;
import core.ResponseEntity;

public class SampleHandlerOne extends AbstractHandler<SamplePayload, SamplePayload> {
    public ResponseEntity<SamplePayload> handleInternal(RequestEntity<SamplePayload> requestEntity) {
        final String value = requestEntity.getBody().getValue();

        if (value.equals("wtf")) {
            throw new SampleException();
        }

        final HandlerTwoClient handlerTwoClient = getDependency(HandlerTwoClient.class);

        final RequestEntity<SamplePayload> handlerTwoRequest = new RequestEntity<SamplePayload>(new SamplePayload(value));
        final ResponseEntity<SamplePayload> handlerTwoResponse = handlerTwoClient.call(handlerTwoRequest);

        return ResponseEntity.of(handlerTwoResponse.getResponseBody(), 200);
    }

    public Class<SamplePayload> getInputClass() {
        return SamplePayload.class;
    }

    public Class<SamplePayload> getOutputClass() {
        return SamplePayload.class;
    }

    public Class<?> getApplicationContextClass() {
        return ContextConfig.class;
    }
}
