package core;

public class ResponseEntity<T> {
    private int responseCode;
    private T responseBody;

    public static <T> ResponseEntity<T> of(T responseBody, int responseCode) {
        return new ResponseEntity<T>(responseCode, responseBody);
    }

    public ResponseEntity(int responseCode, T responseBody) {
        this.responseCode = responseCode;
        this.responseBody = responseBody;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public T getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(T responseBody) {
        this.responseBody = responseBody;
    }
}
