package handler;

import core.RequestProcessingException;

public class SampleException extends RequestProcessingException {
    public int getResponseCode() {
        return 403;
    }

    public String getErrorMessage() {
        return "Sample message";
    }
}
