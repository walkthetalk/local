package client;

import core.RequestEntity;
import core.ResponseEntity;
import handler.SamplePayload;

public interface HandlerTwoClient {
    ResponseEntity<SamplePayload> call(RequestEntity<SamplePayload> input);
}
