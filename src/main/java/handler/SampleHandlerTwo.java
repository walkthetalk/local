package handler;

import core.AbstractHandler;
import core.RequestEntity;
import core.ResponseEntity;

public class SampleHandlerTwo extends AbstractHandler<SamplePayload, SamplePayload> {
    public ResponseEntity<SamplePayload> handleInternal(RequestEntity<SamplePayload> requestEntity) {
        final String value = requestEntity.getBody().getValue();
        return ResponseEntity.of(new SamplePayload(value.toUpperCase()), 200);
    }

    public Class<SamplePayload> getInputClass() {
        return SamplePayload.class;
    }

    public Class<SamplePayload> getOutputClass() {
        return SamplePayload.class;
    }

    public Class<?> getApplicationContextClass() {
        return ContextConfig.class;
    }
}
