package handler;

public class SamplePayload {
    private String value;

    public SamplePayload(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
