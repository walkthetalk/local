package core;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractHandler<I, O> implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private ObjectMapper objectMapper = new ObjectMapper();
    private ApplicationContext applicationContext;

    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent proxyRequest, Context context) {
        initContext();

        final RequestEntity<I> requestEntity = new RequestEntity<I>();

        if (!getOutputClass().equals(Void.class)) {
            try {
                requestEntity.setBody(objectMapper.readValue(proxyRequest.getBody(), getInputClass()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        try {
            final ResponseEntity<O> responseEntity = handleInternal(requestEntity);
            return produceResponseEvent(responseEntity);
        } catch (Exception e) {
            return produceResponseEvent(handleException(e));
        }
    }

    public APIGatewayProxyResponseEvent produceResponseEvent(ResponseEntity<?> responseEntity) {
        final APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();

        responseEvent.setStatusCode(responseEntity.getResponseCode());

        try {
            responseEvent.setBody(objectMapper.writeValueAsString(responseEntity.getResponseBody()));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return responseEvent;
    }

    public void initContext() {
        applicationContext = new AnnotationConfigApplicationContext(getApplicationContextClass());
    }

    private ResponseEntity<Map<String, Object>> handleException(Exception exception) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        int responseCode;

        if (exception instanceof RequestProcessingException) {
            final RequestProcessingException rpe = (RequestProcessingException) exception;
            responseCode = rpe.getResponseCode();
            responseMap.put("error", rpe.getErrorMessage());
        } else {
            responseMap.put("error", exception.getMessage());
            responseCode = 500;
        }

        return new ResponseEntity<Map<String, Object>>(responseCode, responseMap);
    }

    protected <T> T getDependency(Class<T> dependencyClass) {
        return applicationContext.getBean(dependencyClass);
    }

    public abstract ResponseEntity<O> handleInternal(RequestEntity<I> requestEntity);

    public abstract Class<I> getInputClass();

    public abstract Class<O> getOutputClass();

    public abstract Class<?> getApplicationContextClass();


}
