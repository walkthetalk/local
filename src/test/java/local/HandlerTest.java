package local;

import core.RequestEntity;
import core.ResponseEntity;
import handler.SampleException;
import handler.SampleHandlerOne;
import handler.SamplePayload;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class HandlerTest {

    private static SampleHandlerOne sampleHandlerOne;

    @BeforeClass
    public static void prepare() {
        System.setProperty("spring.profiles.active", "local");
        sampleHandlerOne = new SampleHandlerOne();
        sampleHandlerOne.initContext();
    }

    @Test(expected = SampleException.class)
    public void testException() {
        sampleHandlerOne.handleInternal(new RequestEntity<SamplePayload>(new SamplePayload("wtf")));
    }

    @Test
    public void testOK() {
        final RequestEntity<SamplePayload> requestEntity = new RequestEntity<SamplePayload>(new SamplePayload("lol"));
        final ResponseEntity<SamplePayload> responseEntity = sampleHandlerOne.handleInternal(requestEntity);
        Assert.assertEquals("LOL", responseEntity.getResponseBody().getValue());
        Assert.assertEquals(200, responseEntity.getResponseCode());
    }


}
