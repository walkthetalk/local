package handler;

import client.HandlerTwoClient;
import client.HandlerTwoLocalClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ContextConfig {

    @Profile("local")
    @Bean
    public SampleHandlerTwo sampleHandlerTwo() {
        return new SampleHandlerTwo();
    }

    @Profile("local")
    @Bean
    public HandlerTwoClient localHandlerTwoClient() {
        return new HandlerTwoLocalClient(sampleHandlerTwo());
    }


}
